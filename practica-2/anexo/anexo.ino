// Declaración de constantes
#define LDR 3  // Pin al que está conectado el sensor LDR
#define SERIALSPEED 9600 // Velocidad de transmisión del monitor serie
// Configuración de pines y del monitor serie

void setup()
{
  pinMode(LDR, OUTPUT);
  Serial.begin(SERIALSPEED);
}

void loop()
{
  // Lectura del sensor LDR
  int readed = analogRead(LDR);

  // Escritura en el monitor serie del valor convertido leído del sensor
  readed = map(readed, 0, 1023, 100, 0);
  Serial.print("Porcentaje de luz: ");
  Serial.print(readed);
  Serial.print("%");
  Serial.println();
}
