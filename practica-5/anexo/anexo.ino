#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define PIN_SENSOR A0

LiquidCrystal_I2C lcd(0x27, 16, 2);
int estado = -1;

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
}

int hay_objeto() {
  int valor = analogRead(PIN_SENSOR);
  Serial.println(valor);
  return valor >= 231;
}

void objeto_detectado() {
  estado = 1;
  lcd.setCursor(0, 0);
  lcd.print("objeto      ");
  lcd.setCursor(0, 1);
  lcd.print("detectado   ");
}

void objeto_no_detectado() {
  estado = 0;
  lcd.setCursor(0, 0);
  lcd.print("objeto      ");
  lcd.setCursor(0, 1);
  lcd.print("no detectado");
}

void loop() {
  int nuevo_estado = hay_objeto();
  if (nuevo_estado != estado) {
    if (nuevo_estado) {
      objeto_detectado(); 
    } else {
      objeto_no_detectado();
    }
  }
}
