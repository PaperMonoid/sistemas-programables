#include "navigation.h"

Navigation::Navigation(Motor* leftMotor, Motor* rightMotor) {
  this->leftMotor = leftMotor;
  this->rightMotor = rightMotor;
}

void Navigation::init() {
  leftMotor->init();
  rightMotor->init();
}

void Navigation::left() {
  leftMotor->backward(64);
  rightMotor->forward(speed);
}

void Navigation::right() {
  leftMotor->forward(64);
  rightMotor->backward(speed);
}

void Navigation::forward() {
  leftMotor->forward(64);
  rightMotor->forward(speed);
}

void Navigation::backward() {
  leftMotor->backward(64);
  rightMotor->backward(speed);
}

void Navigation::stop() {
  leftMotor->stop();
  rightMotor->stop();
}
