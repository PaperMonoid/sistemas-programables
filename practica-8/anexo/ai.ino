#include "ai.h"

#include <TensorFlowLite.h>
#include "tensorflow/lite/experimental/micro/kernels/all_ops_resolver.h"
#include "tensorflow/lite/experimental/micro/micro_error_reporter.h"
#include "tensorflow/lite/experimental/micro/micro_interpreter.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "tensorflow/lite/version.h"

#include "model_data.h"

// https://github.com/arduino/ArduinoTensorFlowLiteTutorials/blob/master/GestureToEmoji/ArduinoSketches/IMU_Classifier/IMU_Classifier.ino

Ai::Ai(Ultrasonic *ultrasonic) {
  this->ultrasonic = ultrasonic;
  for (int i = 0; i < 16; ++i) {
    for (int j = 0; j < 6; ++j) {
      samples[i][j] = 0.0;
    }
  }
}

void Ai::init() {}

int Ai::predict() {
  for(int i = 0; i < 15; i++) {
    for (int j = 0; j < 6; j++) {
      samples[i][j] = samples[i + 1][j];
    }
  }

  samples[15][0] = ultrasonic->getForward() >= 0;
  samples[15][1] = ultrasonic->getLeft() >= 0;
  samples[15][2] = ultrasonic->getRight() >= 0;
  samples[15][3] = ultrasonic->getForward() > 0;
  samples[15][4] = ultrasonic->getLeft() > 0;
  samples[15][5] = ultrasonic->getRight() > 0;

  if (ultrasonic->getForward() == 0) {
    return 3;
  }
  if (ultrasonic->getRight() == 0) {
    return 1;
  }
  
  return 0;
}

/*
tflite::ErrorReporter* error_reporter = nullptr;
const tflite::Model* model = nullptr;
tflite::MicroInterpreter* interpreter = nullptr;
TfLiteTensor* input = nullptr;
TfLiteTensor* output = nullptr;
int inference_count = 0;

// Create an area of memory to use for input, output, and intermediate arrays.
// Finding the minimum value for your model may require some trial and error.
constexpr int kTensorArenaSize = 32 * 1024;
uint8_t tensor_arena[kTensorArenaSize];

Ai::Ai(Ultrasonic *ultrasonic) {
  this->ultrasonic = ultrasonic;
  for (int i = 0; i < 16; ++i) {
    for (int j = 0; j < 6; ++j) {
      samples[i][j] = 0.0;
    }
  }
}

void Ai::init() {
  static tflite::MicroErrorReporter micro_error_reporter;
  error_reporter = &micro_error_reporter;

  model = tflite::GetModel(g_model_data);
  if (model->version() != TFLITE_SCHEMA_VERSION) {
    error_reporter->Report(
        "Model provided is schema version %d not equal "
        "to supported version %d.",
        model->version(), TFLITE_SCHEMA_VERSION);
    return;
  }

  static tflite::ops::micro::AllOpsResolver resolver;

  static tflite::MicroInterpreter static_interpreter(
      model, resolver, tensor_arena, kTensorArenaSize, error_reporter);
  interpreter = &static_interpreter;

  TfLiteStatus allocate_status = interpreter->AllocateTensors();
  if (allocate_status != kTfLiteOk) {
    error_reporter->Report("AllocateTensors() failed");
    return;
  }

  input = interpreter->input(0);
  output = interpreter->output(0);
}

int Ai::predict() {
  for(int i = 0; i < 15; i++) {
    for (int j = 0; i < 6; j++) {
      samples[i][j] = samples[i + 1][j];
    }
  }

  samples[15][0] = ultrasonic->getForward() >= 0;
  samples[15][1] = ultrasonic->getLeft() >= 0;
  samples[15][2] = ultrasonic->getRight() >= 0;
  samples[15][3] = ultrasonic->getForward() > 0;
  samples[15][4] = ultrasonic->getLeft() > 0;
  samples[15][5] = ultrasonic->getRight() > 0;

  // Read the predicted y value from the model's output tensor
  int n = 0;
  for (int i = 0; i < 16; ++i, ++n) {
    for (int j = 0; j < 6; ++j, ++n) {
      input->data.f[n] = samples[i][j];
    }
  }

  // Run inference, and report any error
  TfLiteStatus invoke_status = interpreter->Invoke();
  if (invoke_status != kTfLiteOk) {
    error_reporter->Report("Invoke failed on x_vals: %f\n",
                           static_cast<double>(0.0));
    return 0;
  }
  
  int max_value = 0;
  int max_index = 0;
  for (int i = 0; i < 5; i++) {
    if (output->data.f[0] > max_value) {
      max_value = max_value;
      max_index = i; 
    }
  }
  
  return max_index;
}*/
