#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

#include <ESP32Servo.h>

#include "navigation.h"
#include "ultrasonic.h"
#include "ai.h"

#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

BLECharacteristic *characteristic;

Motor left(26, 27, 25);
Motor right(12, 14, 13);
Servo servo;

Navigation navigation(&left, &right);
Ultrasonic ultrasonic(&servo, 33, 34);
Ai ai(&ultrasonic);

void setup() {  
  pinMode(18, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(15, OUTPUT);
  
  servo.attach(32);
  
  navigation.init();
  ultrasonic.init();

  Serial.begin(115200);

  ai.init();
  
  Serial.println("Starting BLE work!");

  BLEDevice::init("EVA-01");

  BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);

  BLESecurity *security = new BLESecurity();
  security->setAuthenticationMode(ESP_LE_AUTH_BOND);
  security->setCapability(ESP_IO_CAP_NONE);
  security->setRespEncryptionKey(ESP_BLE_ENC_KEY_MASK | ESP_BLE_ID_KEY_MASK);

  BLEServer *server = BLEDevice::createServer();

  BLEService *service = server->createService(SERVICE_UUID);
  characteristic = service->createCharacteristic(
						 CHARACTERISTIC_UUID,
						 BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE
	);
  characteristic->setValue("0");

  service->start();

  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *advertising = BLEDevice::getAdvertising();
  advertising->addServiceUUID(SERVICE_UUID);
  advertising->setScanResponse(true);
  advertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  advertising->setMinPreferred(0x12);

  BLEDevice::startAdvertising();
  Serial.println("Characteristic defined! Now you can read it in your phone!");
}

std::string onAuto() {
  int action = ai.predict();
  if (action == 1) {
    return "1";
  } else if (action == 2) {
    return "2";
  } else if (action == 3) {
    return "3";
  } else if (action == 4) {
    return "4";
  }
  return "0";
}

void onLeft() {
  navigation.left();
  digitalWrite(18, HIGH);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(15, LOW);
}

void onRight() {
  navigation.right();
  digitalWrite(18, LOW);
  digitalWrite(4, HIGH);
  digitalWrite(5, LOW);
  digitalWrite(15, LOW);
}

void onForward() {
  if (ultrasonic.getForward() > 0) {
    onStop();
  } else {
    navigation.forward();
    digitalWrite(18, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(15, LOW);
  }
}

void onBackward() {
  navigation.backward();
  digitalWrite(18, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(15, LOW);
}

void onStop() {
  navigation.stop();
  digitalWrite(18, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, HIGH);
  digitalWrite(15, HIGH);
}

void loop() {
  delay(100);
  std::string value = characteristic->getValue();
  
  if (value == "5") {
    value = onAuto();
    ultrasonic.enableMovement();
  } else {
    ultrasonic.disableMovement();
  }
  
  ultrasonic.update(100);
  
  if (value == "1") {
    onLeft();
  } else if (value == "2") {
    onRight();
  } else if (value == "3") {
    onForward();
  } else if (value == "4") {
    onBackward();
  } else {
    onStop();
  }
}
