#ifndef NAVIGATION_H
#define NAVIGATION_H

#include "motor.h"

class Navigation {
private:
  Motor* leftMotor;
  Motor* rightMotor;
public:
  int speed = 1023;

  Navigation(Motor* leftMotor, Motor* rightMotor);
  void init();
  void left();
  void right();
  void forward();
  void backward();
  void stop();
};

#endif
