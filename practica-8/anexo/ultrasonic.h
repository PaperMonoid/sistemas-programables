#ifndef ULTRASONIC_H
#define ULTRASONIC_H

#include <ESP32Servo.h>

class Ultrasonic {
private:
  Servo* servo;
  int pin_trigger;
  int pin_echo;

  int time = 0;
  int movement = 0;
  int left = -1;
  int forward = -1;
  int right = -1;
public:
  Ultrasonic(Servo* servo, int pin_trigger, int pin_echo);
  void init();
  void update(int delay);
  void enableMovement();
  void disableMovement();
  int scan();
  int getLeft();
  int getForward();
  int getRight();
};

#endif
