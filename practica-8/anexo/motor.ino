#include "motor.h"

Motor::Motor(int pin_1, int pin_2, int pin_enabled) {
  this->pin_1 = pin_1;
  this->pin_2 = pin_2;
  this->pin_enabled = pin_enabled;
  this->speed = 0;
  this->direction = 1;
}

void Motor::init() {
  pinMode(pin_1, OUTPUT);
  pinMode(pin_2, OUTPUT);
  pinMode(pin_enabled, OUTPUT);
}

void Motor::forward(int speed) {
  if (speed > 1023) {
    speed = 1023;
  } else if (speed < 0) {
    speed = 0;
  }
  this->speed = speed;
  direction = 1;
  digitalWrite(pin_1, HIGH);
  digitalWrite(pin_2, LOW);
  digitalWrite(pin_enabled, speed);
}

void Motor::backward(int speed) {
  if (speed > 1023) {
    speed = 1023;
  } else if (speed < 0) {
    speed = 0;
  }
  this->speed = speed;
  direction = 0;
  digitalWrite(pin_1, LOW);
  digitalWrite(pin_2, HIGH);
  digitalWrite(pin_enabled, speed);
}

void Motor::stop() {
  speed = 0;
  digitalWrite(pin_1, LOW);
  digitalWrite(pin_2, LOW);
  digitalWrite(pin_enabled, speed);
}

int Motor::getSpeed() {
  return speed;
}

int Motor::getDirection() {
  return direction;
}
