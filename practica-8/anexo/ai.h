#ifndef AI_H
#define AI_H

#include "ultrasonic.h"

class Ai {
private:
  float samples[16][6];
  Ultrasonic *ultrasonic;
public:
  Ai(Ultrasonic *ultrasonic);
  void init();
  int predict();
};

#endif
