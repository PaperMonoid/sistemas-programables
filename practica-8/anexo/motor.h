#ifndef MOTOR_H
#define MOTOR_H

class Motor {
private:
  int pin_enabled;
  int pin_1;
  int pin_2;
  int speed = 0;
  int direction = 1;
public:
  Motor(int pin_1, int pin_2, int pin_enabled);
  void init();
  void forward(int speed);
  void backward(int speed);
  void stop();
  int getSpeed();
  int getDirection();
};

#endif
