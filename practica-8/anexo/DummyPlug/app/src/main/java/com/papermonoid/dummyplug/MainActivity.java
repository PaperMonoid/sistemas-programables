package com.papermonoid.dummyplug;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity
        implements BluetoothDisabledFragment.OnFragmentInteractionListener,
        ControlsFragment.OnFragmentInteractionListener {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_CONNECT = 2;
    private static BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        enableBluetoothRequest();
    }

    private void enableBluetoothRequest() {
        if (bluetoothAdapter.isEnabled()) {
            onBluetoothEnabled();
        } else {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                onBluetoothEnabled();
            } else {
                onBluetoothDisabled();
            }
        } else if (requestCode == REQUEST_CONNECT) {
            if (resultCode == RESULT_OK) {
                onConnect(data.getStringExtra("address"));
            } else {
                onDisconnect();
            }
        }
    }

    void onBluetoothEnabled() {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", "ENABLED");
        onDisconnect();
    }

    void onBluetoothDisabled() {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", "DISABLED");

        BluetoothDisabledFragment fragment = new BluetoothDisabledFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameMain, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onEnableBluetoothRequest() {
        enableBluetoothRequest();
    }

    public void onConnect(String address) {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", "CONNECTED TO " + address);

        ControlsFragment fragment = new ControlsFragment(address);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameMain, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onDisconnect() {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", "DISCONNECTED" );
        Intent connectActivityIntent = new Intent(this, ConnectActivity.class);
        startActivityForResult(connectActivityIntent, REQUEST_CONNECT);
    }
}
