package com.papermonoid.dummyplug;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ControlsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ControlsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControlsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private BluetoothAdapter bluetoothAdapter;
    private String address;

    public ControlsFragment() {
        // Required empty public constructor
    }

    public ControlsFragment(String address) {
        this.address = address;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ControlsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ControlsFragment newInstance(String param1, String param2) {
        ControlsFragment fragment = new ControlsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void onGattReady(final BluetoothGatt gatt, View view) {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", gatt.getServices().toString() );
        BluetoothGattService control = gatt.getService(UUID.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b"));
        final BluetoothGattCharacteristic characteristic = control.getCharacteristic(UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8"));

        Button btnForward = (Button) view.findViewById(R.id.btnForward);
        btnForward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "FORWARD" );
                    characteristic.setValue("3");
                    gatt.writeCharacteristic(characteristic);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "STOP" );
                    characteristic.setValue("0");
                    gatt.writeCharacteristic(characteristic);
                }
                return true;
            }
        });
        Button btnBackward = (Button) view.findViewById(R.id.btnBackward);
        btnBackward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "BACKWARD" );
                    characteristic.setValue("4");
                    gatt.writeCharacteristic(characteristic);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "STOP" );
                    characteristic.setValue("0");
                    gatt.writeCharacteristic(characteristic);
                }
                return true;
            }
        });
        Button btnLeft = (Button) view.findViewById(R.id.btnLeft);
        btnLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "LEFT" );
                    characteristic.setValue("1");
                    gatt.writeCharacteristic(characteristic);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "STOP" );
                    characteristic.setValue("0");
                    gatt.writeCharacteristic(characteristic);
                }
                return true;
            }
        });
        Button btnRight = (Button) view.findViewById(R.id.btnRight);
        btnRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "RIGHT" );
                    characteristic.setValue("2");
                    gatt.writeCharacteristic(characteristic);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "STOP" );
                    characteristic.setValue("0");
                    gatt.writeCharacteristic(characteristic);
                }
                return true;
            }
        });
        Button btnAuto = (Button) view.findViewById(R.id.btnAuto);
        btnAuto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "AUTO" );
                    characteristic.setValue("5");
                    gatt.writeCharacteristic(characteristic);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "STOP" );
                    characteristic.setValue("0");
                    gatt.writeCharacteristic(characteristic);
                }
                return true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = (View) inflater.inflate(R.layout.fragment_controls, container, false);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        BluetoothGatt gatt = device.connectGatt(this.getContext(), true, new BluetoothGattCallback() {

            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "CONNECTED TO GATT SERVER" );
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "ATTEMPTING TO START SERVICE DISCOVERY " +
                            gatt.discoverServices());
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Log.println(Log.DEBUG, "BLUETOOTH_STATE", "DISCONNECTED FROM GATT SERVER" );
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                onGattReady(gatt, view);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onDisconnect();
    }
}
