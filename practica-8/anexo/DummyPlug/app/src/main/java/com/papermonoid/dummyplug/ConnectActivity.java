package com.papermonoid.dummyplug;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ConnectActivity extends AppCompatActivity
        implements BluetoothDeviceFragment.OnListFragmentInteractionListener {


    Button btnScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        final BluetoothDeviceFragment fragment = new BluetoothDeviceFragment();

        btnScan = (Button) findViewById(R.id.btnScan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.scanLeDevices();
            }
        });


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameConnect, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void startScan() {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", "START");
        btnScan.setEnabled(false);
    }

    @Override
    public void endScan() {
        Log.println(Log.DEBUG, "BLUETOOTH_STATE", "END");
        btnScan.setEnabled(true);
    }

    @Override
    public void onSelectDevice(BluetoothDevice device) {
        Intent intent = new Intent();
        intent.putExtra("address", device.getAddress());
        setResult(RESULT_OK, intent);
        finish();
    }
}
