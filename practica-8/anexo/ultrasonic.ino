#include "ultrasonic.h"

Ultrasonic::Ultrasonic(Servo* servo, int pin_trigger, int pin_echo) {
  this->servo = servo;
  this->pin_trigger = pin_trigger;
  this->pin_echo = pin_echo;;
}

void Ultrasonic::init() {
  pinMode(pin_trigger, OUTPUT);
  pinMode(pin_echo, INPUT);
}

void Ultrasonic::update(int delay) {
  if (movement) {
    int degrees = 0;
    time += delay;
    if (time == 100) {
      degrees = 0;
      left = scan();
      forward = -1;
      right = -1;
    } else if (time < 1100) {
      degrees = (1 - (1100 - time) / 1100) * 90;
    } else if (time == 1100) {
      degrees = 90;
      left = -1;
      forward = scan();
      right = -1;
    } else if (time < 2100) {
      degrees = 90 + (1 - (2100 - time) / 2100) * 90;;
    } else if (time == 2100) {
      degrees = 180;
      left = -1;
      forward = -1;
      right = scan();
    } else if (time < 3100) {
      degrees = 180 - (1 - (3100 - time) / 3100) * 90;
    } else if (time == 3100) {
      degrees = 90;
      left = -1;
      forward = scan();
      right = -1;
    } else if (time < 4100) {
      degrees = 90 - (1 - (4100 - time) / 4100) * 90;
    } else if (time >= 4100) {
      time = 0;
    }
    servo->write(degrees);
  } else {
    int degrees = 90;
    left = -1;
    forward = scan();
    right = -1;
    servo->write(degrees);
  }
}

int Ultrasonic::scan() {
  digitalWrite(pin_trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(pin_trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(pin_trigger, LOW);

  long duration = pulseIn(pin_echo, HIGH);
  int distance = duration * 0.034 / 2;

  Serial.println(distance);

  return distance < 20;
}

void Ultrasonic::enableMovement() {
  movement = 1;
}

void Ultrasonic::disableMovement() {
  movement = 0;
}

int Ultrasonic::getLeft() {
  return left;
}

int Ultrasonic::getForward() {
  return forward;
}

int Ultrasonic::getRight() {
  return right;
}
