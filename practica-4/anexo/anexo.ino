#define MOTOR_INPUT 8
#define MOTOR_OUTPUT 7
#define REPETITIONS 5
#define ACCEPTING_REPETITIONS 3

int accepting_states[2] = { 3, 4 };
int states[5][2] = {
  1, 2, // q0
  3, 2, // q1
  1, 4, // q2
  3, 2, // q3
  1, 4  // q4
};

int is_accepting(int state) {
  for (int i = 0; i < 2; ++i)
    if (state == accepting_states[i])
      return 1;
  return 0;
}

int next(int state, int input) {
  return states[state][input];
}

void delay_print(const char* str, long amount) {
  for (long i = 0; i < amount; i += 100) {
    delay(100);
    Serial.println(str);
  }
}

int state, times, times_accepting;

void setup()
{
  pinMode(MOTOR_INPUT, INPUT);
  pinMode(MOTOR_OUTPUT, OUTPUT);
  Serial.begin(9600);
  state = 0;
}

void loop()
{
  int input = digitalRead(MOTOR_INPUT);
  ++times;
  if (times >= REPETITIONS) {
    state = next(state, input);
    if (is_accepting(state)) {
      ++times_accepting;
      if (times_accepting == ACCEPTING_REPETITIONS) {
        delay_print("0", 2000);
        digitalWrite(MOTOR_OUTPUT, HIGH);
        delay_print("0", 100);
        digitalWrite(MOTOR_OUTPUT, LOW);
        delay_print("0", 100);
        digitalWrite(MOTOR_OUTPUT, HIGH);
      } else if (times_accepting > ACCEPTING_REPETITIONS) {
        digitalWrite(MOTOR_OUTPUT, HIGH);
        delay_print("0", 100);
        digitalWrite(MOTOR_OUTPUT, LOW);
        delay_print("0", 100);
        digitalWrite(MOTOR_OUTPUT, HIGH);
      } else {
        delay_print("255", 500);
      }
    } else {
      times_accepting = 0;
      delay_print("255", 500);
      digitalWrite(MOTOR_OUTPUT, HIGH);
    }
    times = 0;
  }
}
