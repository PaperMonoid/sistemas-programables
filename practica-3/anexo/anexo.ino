void setup()
{
  Serial.begin(9600);
}

void loop()
{
  int sensorValue = analogRead(A0);
  float temperature = sensorValue * 5.0 * 100.0 / 1024.0 / 5.0;
  Serial.print("Temperature: ");
  Serial.println(temperature);
  delay(1000);
}
