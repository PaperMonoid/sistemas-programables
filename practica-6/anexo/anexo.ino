#define DHT11_PIN 2
#define R_PIN 5 //d1
#define G_PIN 4 //d2
#define B_PIN 0 //d3
#define MOTOR_PIN 16

#define THRESHOLD_1 33.0f
#define THRESHOLD_2 40.0f

typedef enum {
  NORMAL_READING, 
  HIGH_READING,
  DANGER_READING
} dht11_reading;

#include <DHT.h>

DHT dht(DHT11_PIN, DHT11);

void setup() {
  Serial.begin(9600);

  pinMode(R_PIN, OUTPUT);
  pinMode(G_PIN, OUTPUT);
  pinMode(B_PIN, OUTPUT);
  pinMode(MOTOR_PIN, OUTPUT);
  
  dht.begin();
}

dht11_reading get_dht11_reading() {
  float temperature = dht.readTemperature();
  float humidity = dht.readHumidity();
  
  Serial.print("Temperatura = ");
  Serial.print(temperature);
  Serial.print(" ; ");
  Serial.print("Humedad = ");
  Serial.print(humidity);
  Serial.println(" ; ");
  
  if (temperature < THRESHOLD_1) {
    return NORMAL_READING;
  } else if (temperature < THRESHOLD_2) {
    return HIGH_READING;
  } else {
    return DANGER_READING;
  }
}

void loop() {
  dht11_reading reading = get_dht11_reading();
  
  switch (reading) {
    case NORMAL_READING:
      digitalWrite(R_PIN, LOW);
      digitalWrite(G_PIN, LOW);
      digitalWrite(B_PIN,HIGH);
      digitalWrite(MOTOR_PIN, HIGH);
      break;
    case HIGH_READING:
      digitalWrite(R_PIN, LOW);
      digitalWrite(G_PIN, HIGH);
      digitalWrite(B_PIN, LOW);
      digitalWrite(MOTOR_PIN, HIGH);
      break;
    case DANGER_READING:
      digitalWrite(R_PIN, HIGH);
      digitalWrite(G_PIN, LOW);
      digitalWrite(B_PIN, LOW);
      digitalWrite(MOTOR_PIN, LOW);
      break;
    default:
      digitalWrite(R_PIN, LOW);
      digitalWrite(G_PIN, LOW);
      digitalWrite(B_PIN, LOW);
      digitalWrite(MOTOR_PIN, HIGH);
      break;
  }
}
