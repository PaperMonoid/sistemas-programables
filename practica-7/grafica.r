library("tsne")
library("ggplot2")

sensor.data <- read.csv(file="sensor_data.csv", header=TRUE, sep=",")
data <- sensor.data[,1:4]
labels <- sensor.data[,5]

tsne.data <- tsne(data)
tsne.dataframe <- data.frame(x=tsne.data[,1], y=tsne.data[,2], label=labels)

ggplot(tsne.dataframe) +
    ggtitle("t-SNE de los datos del sensor") +
    geom_point(aes(x, y, color=label), size=3) +
    scale_shape_manual(values=seq(0,8)) +
    scale_color_manual(values=c("red"="red", "green"="green", "blue"="blue", "magenta"="magenta", "cyan"="cyan", "yellow"="yellow", "noise"="black", "white"="gray"))
