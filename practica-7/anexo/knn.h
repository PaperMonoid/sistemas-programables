#ifndef KNN_H
#define KNN_H

#include <vector>

typedef std::vector<float> Sample;

struct LabeledDistance;

struct LabeledFrequency;

class KnnClassifier {
private:
  int k;
  std::vector<int> samples_labels;
  std::vector<Sample> samples_data;
public:
  KnnClassifier(int k, std::vector<int> samples_labels,
		std::vector<Sample> samples_data);
  int predict(Sample sample);
};

#endif
