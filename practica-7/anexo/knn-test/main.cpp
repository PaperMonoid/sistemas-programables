#include <iostream>
#include<sstream>
#include "../knn.cpp"

// iris dataset
int main() {
  std::vector<std::string> labels_str
    {
     "Iris-setosa",
     "Iris-versicolor",
     "Iris-virginica",
    };
  std::vector<int> samples_labels
    {
     0, 0, 0, 0, 0,
     1, 1, 1, 1, 1,
     2, 2, 2, 2, 2,
    };
  std::vector<Sample> samples_data
    {
     Sample { 5.1,3.5,1.4,0.2 },
     Sample { 4.9,3.0,1.4,0.2 },
     Sample { 4.7,3.2,1.3,0.2 },
     Sample { 5.0,3.6,1.4,0.2 },
     Sample { 5.4,3.9,1.7,0.4 },

     Sample { 7.0,3.2,4.7,1.4 },
     Sample { 6.4,3.2,4.5,1.5 },
     Sample { 6.9,3.1,4.9,1.5 },
     Sample { 5.5,2.3,4.0,1.3 },
     Sample { 6.5,2.8,4.6,1.5 },

     Sample { 6.3,3.3,6.0,2.5 },
     Sample { 5.8,2.7,5.1,1.9 },
     Sample { 7.1,3.0,5.9,2.1 },
     Sample { 6.3,2.9,5.6,1.8 },
     Sample { 6.5,3.0,5.8,2.2 },

    };

  KnnClassifier classifier(3, samples_labels, samples_data);

  std::string line;
  std::getline(std::cin, line);
  std::stringstream s_stream(line);
  std::string value_1_str, value_2_str, value_3_str, value_4_str, value_label;
  float value_1, value_2, value_3, value_4;
  if(s_stream.good()) {
    getline(s_stream, value_1_str, ',');
    value_1 = ::atof(value_1_str.c_str());
  }
  if(s_stream.good()) {
    getline(s_stream, value_2_str, ',');
    value_2 = ::atof(value_2_str.c_str());
  }
  if(s_stream.good()) {
    getline(s_stream, value_3_str, ',');
    value_3 = ::atof(value_3_str.c_str());
  }
  if(s_stream.good()) {
    getline(s_stream, value_4_str, ',');
    value_4 = ::atof(value_4_str.c_str());
  }
  if(s_stream.good()) {
    getline(s_stream, value_label, ',');
  }

  int label = classifier.predict(Sample { value_1, value_2, value_3, value_4 });

  if (value_label.compare(labels_str[label]) == 0) {
    std::cout << "Correct: " << value_label << std::endl;
  } else {
    std::cout << "INCORRECT!" << value_label << " " << labels_str[label] << std::endl;
  }

  return 0;
}
