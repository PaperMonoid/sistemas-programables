#include "knn.h"

#include <vector>
#include <math.h>
#include <algorithm>

float euclidean_distance(Sample p, Sample q) {
  float sum = 0.0f;
  Sample::iterator p_it = p.begin();
  Sample::iterator q_it = q.begin();
  while (p_it != p.end() && q_it != q.end()) {
    sum += pow(*p_it - *q_it, 2.0f);
    ++p_it;
    ++q_it;
  }
  return sqrt(sum);
}

struct LabeledDistance {
  int label;
  float distance;
};

bool compare_distance(LabeledDistance a, LabeledDistance b) {
  return a.distance < b.distance;
}

struct LabeledFrequency {
  int label;
  int frequency;
};

std::vector<LabeledFrequency>::iterator find_label(std::vector<LabeledFrequency>::iterator it,
						   std::vector<LabeledFrequency>::iterator limit,
						   int label) {
  while (it != limit) {
    if (it->label == label) {
      return it;
    }
    ++it;
  }
  return it;
}

bool compare_frequency(LabeledFrequency a, LabeledFrequency b) {
  return a.frequency > b.frequency;
}

KnnClassifier::KnnClassifier(int k, std::vector<int> samples_labels,
			     std::vector<Sample> samples_data)
{
  this->k = k;
  this->samples_labels = samples_labels;
  this->samples_data = samples_data;
}

int KnnClassifier::predict(Sample sample) {
  std::vector<LabeledDistance> distances(samples_data.size(), { 0, 0.0f });

  std::vector<int>::iterator samples_labels_it;
  std::vector<Sample>::iterator samples_data_it;
  std::vector<LabeledDistance>::iterator distances_it;

  samples_labels_it = samples_labels.begin();
  samples_data_it = samples_data.begin();
  distances_it = distances.begin();
  while (samples_labels_it != samples_labels.end() &&
	 samples_data_it != samples_data.end() &&
	 distances_it != distances.end()) {

    distances_it->label = *samples_labels_it;
    distances_it->distance = euclidean_distance(sample, *samples_data_it);

    ++samples_labels_it;
    ++samples_data_it;
    ++distances_it;
  }

  std::sort(distances.begin(), distances.end(), compare_distance);

  std::vector<LabeledFrequency> frequencies;

  std::vector<LabeledFrequency>::iterator frequencies_it;

  distances_it = distances.begin();
  while (distances_it != distances.begin() + k) {
    frequencies_it = find_label(frequencies.begin(), frequencies.end(),
				distances_it->label);

    if (frequencies_it != frequencies.end()) {
      frequencies_it->frequency++;
    } else {
      frequencies.insert(frequencies_it, { distances_it->label, 1 });
    }

    ++distances_it;
  }

  std::sort(frequencies.begin(), frequencies.end(), compare_frequency);

  if (frequencies.empty()) {
    return 0;
  } else {
    return frequencies.front().label;
  }
}
