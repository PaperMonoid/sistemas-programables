const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    main: "./src/index.jsx"
  },
  output: {
    filename: "./[name].min.js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/i,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        loader: "react-svg-loader"
      }
    ]
  },
  plugins: [new CopyPlugin([{ from: "src/*.html", to: "[name].html" }])],
  externals: {
    react: "React",
    "react-dom": "ReactDOM"
  }
};
