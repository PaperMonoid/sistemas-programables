#!/bin/bash

if [ -d embedded ]
then
    rm -Rf embedded/*
else
    mkdir embedded
fi

if [ -d dist ]
then
    for filename in dist/*
    do
	embedded_filename=$(basename $filename)
	xxd -i $filename > "embedded/${embedded_filename}.h"
	sed -i "s/};/,0x00};/" "embedded/${embedded_filename}.h"
	embedded_filesize=$(awk '/= [0-9]+;/{ print substr($5, 1, length($5) - 1)}' "embedded/${embedded_filename}.h")
	embedded_new_filesize=$(($embedded_filesize + 1))
	sed -i "s/= ${embedded_filesize};/= ${embedded_new_filesize};/" "embedded/${embedded_filename}.h"

	echo "DONE ${embedded_filename}"
    done
fi
