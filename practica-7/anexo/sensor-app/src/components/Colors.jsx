import React, { Component } from "react";
import ReactDOM from "react-dom";

import styles from "./Colors.css";
import ColorsImage from "../media/colors.min.svg";

import Color from "./Color.jsx";

function Colors(props) {
  const { available, current, previous } = props;
  return (
    <div className={available ? styles.available : styles.notAvailable}>
      <ColorsImage width={640} height={640} />
      <div className={styles.colorRed}>
        <Color colorName="Red" previous={previous.red} current={current.red} />
      </div>
      <div className={styles.colorGreen}>
        <Color
          colorName="Green"
          previous={previous.green}
          current={current.green}
        />
      </div>
      <div className={styles.colorBlue}>
        <Color
          colorName="Blue"
          previous={previous.blue}
          current={current.blue}
        />
      </div>
      <div className={styles.colorCyan}>
        <Color
          colorName="Cyan"
          previous={previous.cyan}
          current={current.cyan}
        />
      </div>
      <div className={styles.colorMagenta}>
        <Color
          colorName="Magenta"
          previous={previous.magenta}
          current={current.magenta}
        />
      </div>
      <div className={styles.colorYellow}>
        <Color
          colorName="Yellow"
          previous={previous.yellow}
          current={current.yellow}
        />
      </div>
      <div className={styles.colorWhite}>
        <Color
          colorName="White"
          previous={previous.white}
          current={current.white}
        />
      </div>
    </div>
  );
}

export default Colors;
