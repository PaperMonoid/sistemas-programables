import React, { Component } from "react";
import ReactDOM from "react-dom";

import styles from "./Loading.css";
import Esp32Image from "../media/esp32.min.svg";

function Loading(props) {
  return (
    <div className={styles.loading}>
      <div className={styles.esp32}>
        <Esp32Image width={200} height={400} />
      </div>
    </div>
  );
}

export default Loading;
