import React, { Component } from "react";
import ReactDOM from "react-dom";

import styles from "./App.css";

import Colors from "./Colors.jsx";
import Loading from "./Loading.jsx";
import { getColorsCount, resetColorsCount } from "../models/ColorsCount.js";

class App extends Component {
  constructor(props) {
    super(props);

    this.empty = {
      red: 0,
      green: 0,
      blue: 0,
      cyan: 0,
      magenta: 0,
      yellow: 0,
      white: 0
    };

    this.state = {
      sampleRate: 2000,
      date: new Date().toString(),
      loading: true,
      available: false,
      previous: this.empty,
      current: this.empty
    };

    this.getColorsCount(true);
  }

  loadAndGetColorsCount() {
    this.setState({ loading: true });
    this.getColorsCount(true);
  }

  loadAndResetColorsCount() {
    this.setState({ loading: true });
    this.resetColorsCount();
  }

  getColorsCount(reset) {
    getColorsCount()
      .then(
        function(colors) {
          if (this.state.loading && !reset) {
            throw new Error("CANCELLED");
          } else {
            return colors;
          }
        }.bind(this)
      )
      .then(
        function(colors) {
          this.onGetColorsCount(colors);
          setTimeout(this.getColorsCount.bind(this), this.state.sampleRate);
        }.bind(this)
      )
      .catch(
        function(error) {
          if (!(error && error.message == "CANCELLED"))
            this.onConnectionUnavailable();
        }.bind(this)
      );
  }

  resetColorsCount() {
    resetColorsCount()
      .then(this.onResetColorsCount.bind(this))
      .catch(this.onConnectionUnavailable.bind(this));
  }

  onGetColorsCount(colors) {
    let equal = true;
    for (let key in colors) {
      equal &= colors[key] == this.state.current[key];
    }
    this.setState({
      loading: false,
      available: true,
      date: equal ? this.state.date : new Date().toString(),
      previous: this.state.current,
      current: colors
    });
  }

  onResetColorsCount() {
    this.setState({
      loading: true,
      available: false,
      date: new Date().toString(),
      previous: this.empty,
      current: this.empty
    });
    this.getColorsCount(true);
  }

  onConnectionUnavailable() {
    this.setState({
      loading: false,
      available: false,
      date: new Date().toString(),
      previous: this.empty,
      current: this.empty
    });
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    } else {
      return (
        <div>
          <div className={styles.origin}>Origin: {location.origin}</div>
          <div className={styles.date}>Date: {this.state.date}</div>
          <div className={styles.colorsContainer}>
            <Colors
              available={this.state.available}
              current={this.state.current}
              previous={this.state.previous}
            />
          </div>
          {this.state.available ? (
            <button
              className={styles.reset}
              onClick={this.loadAndResetColorsCount.bind(this)}
            >
              Reset
            </button>
          ) : (
            <button
              className={styles.connect}
              onClick={this.loadAndGetColorsCount.bind(this)}
            >
              Connect
            </button>
          )}
        </div>
      );
    }
  }
}

export default App;
