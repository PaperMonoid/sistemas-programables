import React, { Component } from "react";
import ReactDOM from "react-dom";

import styles from "./Color.css";

function Color(props) {
  const { colorName, previous, current } = props;
  const delta = Math.abs(current - previous);
  return (
    <div className={delta ? styles.changed : styles.notChanged}>
      <div>{colorName}</div>
      <div>{current}</div>
    </div>
  );
}

export default Color;
