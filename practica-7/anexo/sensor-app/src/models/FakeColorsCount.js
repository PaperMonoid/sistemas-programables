let colors = {
  red: 0,
  green: 0,
  blue: 0,
  cyan: 0,
  magenta: 0,
  yellow: 0,
  white: 0
};

setInterval(function() {
  colors.red += Math.random() > 0.9;
  colors.green += Math.random() > 0.9;
  colors.blue += Math.random() > 0.9;
  colors.cyan += Math.random() > 0.9;
  colors.magenta += Math.random() > 0.9;
  colors.yellow += Math.random() > 0.9;
  colors.white += Math.random() > 0.9;
}, 1000);

function getColorsCount() {
  return new Promise(function(accept, reject) {
    setTimeout(function() {
      if (Math.random() > 0.9) {
        reject();
      } else {
        accept(Object.assign({}, colors));
      }
    }, 3000);
  });
}

function resetColorsCount() {
  return new Promise(function(accept, reject) {
    setTimeout(function() {
      if (Math.random() > 0.3) {
        reject();
      } else {
        colors.red = 0;
        colors.green = 0;
        colors.blue = 0;
        colors.cyan = 0;
        colors.magenta = 0;
        colors.yellow = 0;
        colors.white = 0;
        accept(Object.assign({}, colors));
      }
    }, 3000);
  });
}

export { getColorsCount, resetColorsCount };
