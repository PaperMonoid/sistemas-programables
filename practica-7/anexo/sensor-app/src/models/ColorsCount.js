function tryGetColorsCount() {
  return fetch("/color").then(function(response) {
    return response.json();
  });
}

function getColorsCount() {
  return new Promise(function(accept, reject) {
    tryGetColorsCount()
      .then(accept)
      .catch(function(error) {
        tryGetColorsCount()
          .then(accept)
          .catch(function(error) {
            tryGetColorsCount()
              .then(accept)
              .catch(reject);
          });
      });
  });
}

function tryResetColorsCount() {
  return fetch("/reset").then(function(response) {
    return response.json();
  });
}

function resetColorsCount() {
  return new Promise(function(accept, reject) {
    tryResetColorsCount()
      .then(accept)
      .catch(function(error) {
        tryResetColorsCount()
          .then(accept)
          .catch(function(error) {
            tryResetColorsCount()
              .then(accept)
              .catch(reject);
          });
      });
  });
}

export { getColorsCount, resetColorsCount };
