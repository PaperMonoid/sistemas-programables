#include <WiFi.h>
#include <WebServer.h>
#include "Adafruit_TCS34725.h"

#include "settings.h"
#include "sensor-app/embedded/main.min.js.h"
#include "sensor-app/embedded/index.html.h"
#include "sensor-app/embedded/not-found.html.h"
#include "knn.h"

#define R_PIN 25
#define G_PIN 33
#define B_PIN 32

#define REPETITIONS 2

std::vector<int> colors_counters = {

  // red
  0,

  // green
  0,

  // blue
  0,

  // cyan
  0,

  // magenta
  0,

  // yellow
  0,

  // white
  0
};

std::vector<int> samples_labels = {
  // noise
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,

  // red
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,

  // green
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,

  // blue
  3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,

  // cyan
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,

  // magenta
  5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,

  // yellow
  6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,

  // white
  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
};

std::vector<Sample> samples_data
{
  // noise
  Sample { 82, 77, 64, 228 },
  Sample { 104, 99, 85, 294 },
  Sample { 111, 102, 86, 304 },
  Sample { 88, 86, 73, 254 },
  Sample { 77, 61, 48, 188 },
  Sample { 702, 948, 874, 2641 },
  Sample { 703, 949, 875, 2644 },
  Sample { 691, 932, 859, 2596 },
  Sample { 653, 876, 807, 2444 },
  Sample { 695, 934, 861, 2605 },
  Sample { 583, 716, 548, 1893 },
  Sample { 583, 715, 546, 1892 },
  Sample { 583, 715, 546, 1891 },
  Sample { 583, 715, 546, 1891 },
  Sample { 583, 715, 546, 1891 },
  Sample { 1024, 1229, 973, 3343 },
  Sample { 1025, 1230, 974, 3346 },
  Sample { 1025, 1231, 975, 3349 },
  Sample { 1025, 1230, 974, 3347 },
  Sample { 1024, 1229, 973, 3343 },

  // red
  Sample { 492, 202, 175, 861 },
  Sample { 2486, 1003, 878, 4351 },
  Sample { 2794, 1172, 1040, 5037 },
  Sample { 2722, 1152, 1023, 4947 },
  Sample { 3252, 1362, 1182, 5832 },
  Sample { 1476, 592, 524, 2558 },
  Sample { 1326, 533, 473, 2303 },
  Sample { 1074, 436, 388, 1877 },
  Sample { 843, 354, 314, 1495 },
  Sample { 910, 378, 335, 1603 },
  Sample { 2824, 1188, 1022, 5016 },
  Sample { 3208, 1375, 1178, 5758 },
  Sample { 3379, 1470, 1252, 6111 },
  Sample { 3455, 1514, 1288, 6276 },
  Sample { 3514, 1547, 1314, 6401 },

  // green
  Sample { 1876, 3389, 2128, 7875 },
  Sample { 1853, 3373, 2116, 7838 },
  Sample { 1855, 3376, 2119, 7845 },
  Sample { 1866, 3385, 2125, 7866 },
  Sample { 1876, 3389, 2129, 7876 },
  Sample { 1941, 3402, 2145, 7910 },
  Sample { 1864, 3142, 1997, 7339 },
  Sample { 1307, 1973, 1284, 4695 },
  Sample { 1301, 1965, 1286, 4684 },
  Sample { 1404, 2139, 1394, 5086 },
  Sample { 1816, 2946, 1912, 6965 },
  Sample { 1833, 2986, 1937, 7058 },
  Sample { 1873, 3086, 1997, 7280 },
  Sample { 1903, 3177, 2049, 7476 },
  Sample { 1922, 3245, 2088, 7620 },

  // blue
  Sample { 534, 740, 1045, 2336 },
  Sample { 596, 828, 1171, 2614 },
  Sample { 556, 765, 1078, 2413 },
  Sample { 545, 750, 1056, 2365 },
  Sample { 727, 997, 1401, 3143 },
  Sample { 866, 1208, 1703, 3804 },
  Sample { 1011, 1443, 2045, 4542 },
  Sample { 980, 1385, 1956, 4352 },
  Sample { 949, 1332, 1876, 4177 },
  Sample { 782, 1080, 1516, 3390 },
  Sample { 747, 1122, 1603, 3517 },
  Sample { 850, 1301, 1864, 4069 },
  Sample { 853, 1305, 1860, 4073 },
  Sample { 923, 1446, 2060, 4504 },
  Sample { 947, 1500, 2141, 4671 },

  // cyan
  Sample { 1257, 1766, 1432, 4565 },
  Sample { 2149, 3598, 2877, 9085 },
  Sample { 1957, 3939, 3179, 9916 },
  Sample { 2369, 4430, 3528, 11109 },
  Sample { 2578, 4339, 3426, 10945 },
  Sample { 2568, 4009, 3176, 10176 },
  Sample { 2654, 4239, 3351, 10737 },
  Sample { 2560, 3996, 3165, 10137 },
  Sample { 2542, 3941, 3123, 10004 },
  Sample { 2539, 3921, 3108, 9958 },
  Sample { 2153, 3108, 2458, 7947 },
  Sample { 2364, 3459, 2726, 8820 },
  Sample { 2362, 3449, 2710, 8782 },
  Sample { 2395, 3508, 2755, 8928 },
  Sample { 2394, 3506, 2753, 8922 },

  // magenta
  Sample { 2092, 1144, 1424, 4635 },
  Sample { 2762, 1782, 2287, 6983 },
  Sample { 2915, 1874, 2366, 7307 },
  Sample { 2322, 1385, 1715, 5454 },
  Sample { 2897, 1803, 2240, 7039 },
  Sample { 2786, 1673, 2065, 6567 },
  Sample { 2718, 1623, 2002, 6378 },
  Sample { 2552, 1501, 1842, 5898 },
  Sample { 2466, 1441, 1767, 5673 },
  Sample { 2836, 1695, 2084, 6646 },
  Sample { 1323, 733, 891, 2921 },
  Sample { 1430, 793, 965, 3161 },
  Sample { 1522, 845, 1029, 3368 },
  Sample { 1809, 1015, 1235, 4030 },
  Sample { 1452, 807, 982, 3215 },

  // yellow
  Sample { 4100, 3949, 2530, 10978 },
  Sample { 4059, 3871, 2493, 10782 },
  Sample { 4109, 4612, 2964, 11432 },
  Sample { 4643, 4569, 2951, 12640 },
  Sample { 4607, 4518, 2916, 12508 },
  Sample { 4665, 4598, 2963, 12714 },
  Sample { 4446, 4335, 2805, 12017 },
  Sample { 4582, 4499, 2905, 12451 },
  Sample { 4882, 4897, 3137, 13492 },
  Sample { 4643, 4585, 2952, 12670 },
  Sample { 2983, 2796, 1818, 7822 },
  Sample { 3422, 3257, 2108, 9067 },
  Sample { 3385, 3212, 2080, 8949 },
  Sample { 2868, 2700, 1756, 7548 },
  Sample { 2042, 1902, 1246, 5340 },

  // white
  Sample { 5794, 6623, 5471, 18605 },
  Sample { 6355, 7424, 6107, 20777 },
  Sample { 6460, 7548, 6205, 21125 },
  Sample { 6479, 7565, 6225, 21173 },
  Sample { 6072, 6954, 5743, 19504 },
  Sample { 6057, 6985, 5761, 19585 },
  Sample { 4206, 4661, 3883, 13182 },
  Sample { 6284, 7415, 6093, 20722 },
  Sample { 4684, 5242, 4353, 14795 },
  Sample { 6110, 7134, 5873, 19964 },
  Sample { 5042, 5675, 4678, 15951 },
  Sample { 4202, 4678, 3869, 13190 },
  Sample { 4056, 4503, 3731, 12716 },
  Sample { 4346, 4842, 4007, 13658 },
  Sample { 4546, 5080, 4200, 14316 },
};

KnnClassifier classifier(5, samples_labels, samples_data);

uint16_t r, g, b, c;
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);

const char* ssid = ESP32_SSID;
const char* password = ESP32_PASSWORD;

WebServer server(80);

void handle_home() {
  Serial.println("GET HOME");
  server.send(200, "text/html", String((char*) dist_index_html)); 
}

void handle_color() {
  Serial.println("GET COLOR");
  
  char buffer[1024];
  sprintf(buffer, "{\"red\":%d,\"green\":%d,\"blue\":%d,\"cyan\":%d,\"magenta\":%d,\"yellow\":%d,\"white\":%d}", 
  colors_counters[0],colors_counters[1],colors_counters[2],colors_counters[3],colors_counters[4],colors_counters[5],
  colors_counters[6]);
  server.send(200, "application/json", buffer); 
}

void handle_reset() {
  Serial.println("GET RESET");

  for (std::vector<int>::iterator it = colors_counters.begin(); it != colors_counters.end(); ++it) {
    *it = 0;
  }
  
  server.send(200, "application/json", "{}"); 
}

void handle_main_min_js() {
  Serial.println("GET MAIN JAVASCRIPT");
  server.send(200, "text/javascript", String((char*) dist_main_min_js)); 
}

void handle_not_found() {
  Serial.println("GET NOT FOUND");
  server.send(404, "text/html", String((char*) dist_not_found_html)); 
}

void setup() {
  pinMode(R_PIN, OUTPUT);
  pinMode(G_PIN, OUTPUT);
  pinMode(B_PIN, OUTPUT);

  digitalWrite(R_PIN, HIGH);
  
  Serial.begin(9600);
  Serial.println("Starting serial communication...");
  
  
  Serial.println("Connecting to wifi...");
    
  WiFi.begin(ssid, password);

  int timeout = 3;
  while (WiFi.status() != WL_CONNECTED and timeout-- >= 0)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  
  Serial.print("LOCAL IP ADDRESS: ");
  Serial.println(WiFi.localIP());

  digitalWrite(R_PIN, LOW);
  digitalWrite(G_PIN, HIGH);
  
  Serial.println("Setting up server...");
   
  server.on("/", handle_home);
  server.on("/color", handle_color);
  server.on("/reset", handle_reset);
  server.on("/main.min.js", handle_main_min_js);
  server.onNotFound(handle_not_found);


  if (tcs.begin()) {
    Serial.println("Found sensor!");
  } else {
    Serial.println("No TCS34725 found ... check your connections...");
    while (1);
  }
  
  server.begin();
  Serial.println("Server listening on port 80...");
  
  digitalWrite(G_PIN, LOW);
  digitalWrite(B_PIN, HIGH);
}

void onNoise() {
  Serial.println("NO OBJECT DETECTED");
  digitalWrite(R_PIN, LOW);
  digitalWrite(G_PIN, LOW);
  digitalWrite(B_PIN, LOW);
}

void onRed() {
  Serial.println("RED OBJECT DETECTED");
  colors_counters[0]++;
  digitalWrite(R_PIN, HIGH);
  digitalWrite(G_PIN, LOW);
  digitalWrite(B_PIN, LOW);
}

void onGreen() {
  Serial.println("GREEN OBJECT DETECTED");
  colors_counters[1]++;
  digitalWrite(R_PIN, LOW);
  digitalWrite(G_PIN, HIGH);
  digitalWrite(B_PIN, LOW);
}

void onBlue() {
  Serial.println("BLUE OBJECT DETECTED");
  colors_counters[2]++;
  digitalWrite(R_PIN, LOW);
  digitalWrite(G_PIN, LOW);
  digitalWrite(B_PIN, HIGH);
}

void onCyan() {
  Serial.println("CYAN OBJECT DETECTED");
  colors_counters[3]++;
  digitalWrite(R_PIN, LOW);
  digitalWrite(G_PIN, HIGH);
  digitalWrite(B_PIN, HIGH);
}

void onMagenta() {
  Serial.println("MAGENTA OBJECT DETECTED");
  colors_counters[4]++;
  digitalWrite(R_PIN, HIGH);
  digitalWrite(G_PIN, LOW);
  digitalWrite(B_PIN, HIGH);
}

void onYellow() {
  Serial.println("YELLOW OBJECT DETECTED");
  colors_counters[5]++;
  digitalWrite(R_PIN, HIGH);
  digitalWrite(G_PIN, HIGH);
  digitalWrite(B_PIN, LOW);
}

void onWhite() {
  Serial.println("WHITE OBJECT DETECTED");
  colors_counters[6]++;
  digitalWrite(R_PIN, HIGH);
  digitalWrite(G_PIN, HIGH);
  digitalWrite(B_PIN, HIGH);
}

int last_label = -1;
int label_readings[REPETITIONS];
int label_readings_index = 0;

void loop() {
  
  server.handleClient();

  tcs.getRawData(&r, &g, &b, &c);

  Sample sample { r, g, b, c };
  
  label_readings[label_readings_index] = classifier.predict(sample);
  
  server.handleClient();
  
  int label = label_readings[0];
  int confirm = 1;
  for (int i = 0; i < REPETITIONS; ++i) {
    confirm &= label == label_readings[i];
  }
  
  server.handleClient();
  
  if (confirm && last_label != label) {
    last_label = label;
    switch(label) {
      case 0:
      onNoise();
      break;
      case 1:
      onRed();
      break;
      case 2:
      onGreen();
      break;
      case 3:
      onBlue();
      break;
      case 4:
      onCyan();
      break;
      case 5:
      onMagenta();
      break;
      case 6:
      onYellow();
      break;
      case 7:
      onWhite();
      break;
      default:
      onNoise();
      break;
    }
  }
  
  ++label_readings_index;
  label_readings_index %= REPETITIONS;
}
